package me.monmcgt.code.hm

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default

class Args(parse: ArgParser) {
    val listId by parse.flagging(names = arrayOf("-l", "--list-id"), help = "list all ids")
    val id by parse.storing(names = arrayOf("-i", "--id"), help = "id of history").default(null)
    val file by parse.storing(names = arrayOf("-f", "--file"), help = "file of the history if id is not specified").default(null)
    val outputDelimiter by parse.storing(names = arrayOf("-d", "--delimiter"), help = "delimiter for output").default("\n")
    val add by parse.storing(names = arrayOf("-a", "--add"), help = "add to history").default(null)
    val remove by parse.storing(names = arrayOf("-r", "--remove"), help = "remove from history").default(null)
    val clear by parse.flagging(names = arrayOf("-c", "--clear"), help = "clear history")
    val maxHistory by parse.storing(names = arrayOf("-m", "--max-history"), help = "max history size") { toInt() }.default(300)
}