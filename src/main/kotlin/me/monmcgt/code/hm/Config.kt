package me.monmcgt.code.hm

data class Config(
    val entries: MutableList<Entry>,
) {
    fun increment() {
        entries.forEach { entry ->
            entry.count.replaceAll { it + 1 }
        }
    }

    fun removeCountMoreThan(maxCount: Int) {
        entries.forEach { entry ->
            entry.count.removeAll { it > maxCount }
        }
    }

    fun isCountMissing(): Boolean {
        val intList = entries.flatMap { it.count }
        return (1..intList.max()).any { !intList.contains(it) }
    }

    fun getMissingCount(): List<Int> {
        val intList = entries.flatMap { it.count }
        return (1..intList.max()).filter { !intList.contains(it) }
    }

    fun regenerateCount() {
        // for example, A: [1, 5, 8] B: [2, 6] C: [3, 9]
        // if number before is missing, decrement all numbers after it,
        // so it becomes A: [1, 4, 6] B: [2, 5] C: [3, 7]

        entries.forEach { entry ->
            val count = entry.count
            val missingCount = getMissingCount()
            missingCount.sortedDescending().forEach { missing ->
                count.replaceAll { if (it > missing) it - 1 else it }
            }
        }

        if (isCountMissing()) {
            regenerateCount()
        }
    }

    data class Entry(
        val count: MutableList<Int>,
        val string: String,
    ) {
        fun getCount(): Int {
            return count.size
        }
    }
}
