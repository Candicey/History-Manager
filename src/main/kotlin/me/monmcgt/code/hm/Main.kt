@file:JvmName("Main")

package me.monmcgt.code.hm

import com.xenomachina.argparser.ArgParser
import java.io.File
import kotlin.properties.Delegates

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        var jsonFile by Delegates.notNull<File>()

        ArgParser(args).parseInto(::Args).run {
            if (listId) {
                val directory = File(getSystemConfigurationPath(), "history-manager/history")

                if (directory.exists()) {
                    directory.listFiles()?.forEach {
                        if (it.name.endsWith(".json")) {
                            println(it.nameWithoutExtension)
                        }
                    }
                }
                return
            }

            if (id == null && file == null) {
                println("Either id or file must be specified.")
                return
            }
            jsonFile = if (file != null) {
                if (id != null) {
                    println("Ignoring id because file is specified.")
                }

                File(file!!)
            } else {
                if (!checkValidString(id!!)) {
                    println("Invalid characters in id.")
                    return
                }
                if (id!!.length > 50) {
                    println("Id is too long. Max length is 50 characters.")
                    return
                }

                File("${getSystemConfigurationPath()}/history-manager/history/$id.json")
            }

            if (!jsonFile.exists()) {
                jsonFile.parentFile?.mkdirs()
                jsonFile.createNewFile()
            }

            val config = gson.fromJson(jsonFile.readText(), Config::class.java) ?: Config(mutableListOf())
            val entries = config.entries

            if (add != null) {
                if (add!!.length > 500) {
                    println("String is too long. Max length is 500 characters.")
                    return
                }

                config.increment()
                config.removeCountMoreThan(maxHistory)

                val entry = entries.find { it.string == add }
                entry?.count?.add(1) ?: entries.add(Config.Entry(mutableListOf(1), add!!))

                jsonFile.writeText(gson.toJson(config))
            } else if (remove != null) {
                if (remove!!.length > 500) {
                    println("String is too long. Max length is 500 characters.")
                    return
                }

                entries.removeIf { it.string == remove }
                config.regenerateCount()

                jsonFile.writeText(gson.toJson(config))
            } else if (clear) {
                jsonFile.writeText("")
            }
            else {
                println(entries.sortedByDescending { it.getCount() }.joinToString(outputDelimiter) { it.string })
            }
        }
    }
}
