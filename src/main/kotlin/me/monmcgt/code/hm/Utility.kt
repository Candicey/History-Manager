package me.monmcgt.code.hm

import java.io.File

val acceptableCharsArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_- ".toCharArray()

fun getSystemConfigurationPath(): File {
    val xdgConfigHome = System.getenv("XDG_CONFIG_HOME")
    return if (xdgConfigHome != null) {
        File(xdgConfigHome)
    } else {
        File(System.getProperty("user.home"), ".config")
    }
}

/**
 * @return true if the string is acceptable, false otherwise
 */
fun checkValidString(string: String, acceptableChars: CharArray = acceptableCharsArray): Boolean {
    for (char in string) {
        if (!acceptableChars.contains(char)) {
            return false
        }
    }
    return true
}